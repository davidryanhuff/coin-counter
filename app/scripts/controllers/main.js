'use strict';

/**
 * @ngdoc function
 * @name kenzanCurrencyApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the kenzanCurrencyApp
 */
angular.module('kenzanCurrencyApp')
  .controller('MainCtrl', ['$scope', function ($scope)  {

    // create the coins object with the value of each coin
    $scope.coins = [
        {'denomination' : '£2', 'value': 200},
        {'denomination' : '£1', 'value': 100},
        {'denomination' : '50 pence', 'value': 50},
        {'denomination' : '20 pence', 'value': 20},
        {'denomination' : '2 pence', 'value': 2},
        {'denomination' : '1 pence', 'value': 1}
    ];

    // initialize totals on the page to zero
    $scope.total = '0.00';
    $scope.coinsNeeded = 0;

    // this function is called every time inputs change
    $scope.calculateCoinsNeeded = function() {

      // set values for calculation to zero if angular-model values are undefined/null/NaN
      var poundsInputValue = $scope.pounds || 0,
          penceInputValue = $scope.pence || 0,
          amountEntered = (poundsInputValue * 100) + penceInputValue;

      // caluclate the total amount entered in (£) pounds
      $scope.total = poundsInputValue + (penceInputValue * 0.01);

      // loop through each coin from largest denomination to smallest
      // divide total amount by coin value, pass remainder to next coin
      for (var i = 0; i < $scope.coins.length; i++) {
        // append numerof coins needed to object for current calculation
        $scope.coins[i].numberNeeded = Math.floor(amountEntered / $scope.coins[i].value);
        Math.round(((amountEntered %= $scope.coins[i].value) * 100) / 100);
      }

      // after determining solutions calculate total number of coins
      $scope.coinsNeeded = $scope.getTotal();
    };

    $scope.getTotal = function() {
      var total = 0;
      for(var i = 0; i < $scope.coins.length; i++){
          total += $scope.coins[i].numberNeeded;
      }
      return total;
    };

  }]);
