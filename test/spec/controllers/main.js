'use strict';

describe('Controller: MainCtrl', function () {

  // load the controller's module
  beforeEach(module('kenzanCurrencyApp'));

  var MainCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MainCtrl = $controller('MainCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should contain an object with at least one coin', function () {
    expect(scope.coins.length).toBeGreaterThan(0);
  });

  it('should have a coin with denomination, value', function() {
    for (var i = 0; i < scope.coins.length; i++) {
      expect(scope.coins[i].denomination).not.toBeNull();
      expect(scope.coins[i].value).toBeGreaterThan(0);
    }
  });
});
