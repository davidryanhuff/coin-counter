# Coin Counter

## Requirements
* Calculate the minimum number of Sterling coins needed to add up to a given input
* The user interface should consist of an input field that accepts an 'amount' string (Eg. 92p, £2.12) and displays the denominations needed when the user hits 'enter'.

* Account for only the common £2, £1, 50p, 20p, 2p and 1p coins. Ignore £5 coins.

* You are limited to using use JavaScript, CSS and HTML to do this. Your solution must use AngularJS, but may employ other Javascript libraries as needed.

* The running application may not use server-side code or browser plugins. You may use node.js and related libraries to prepare or create the files needed to run the app (build, etc.).

* All the files required to run the app should be created.

* The application must work in the latest version of Chrome, Firefox, E10+, and on iOS (phone; Chrome or Safari) or Android (any phone; Chrome)

## Solution
* Eliminate the need for an enter button, live calculate user input
* Expecting a user to type currency symbols could lead to a poor user experience. Alternate solution: separate out pounds and pence input, allow for one or both to contain values and perform the necessary calculations. This solution improves usability and reduces maintainability for validation of inputs. is additionally more extensible if different coin types need to be added in the future.


## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.
